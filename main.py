from get_content import Content
from write_docs import Write

content: Content = Content()
write: Write = Write()

if __name__ == '__main__':
    idea = content.get_ideas(topic="Javascript functions")
    intro = content.get_intro(title=idea)
    outline = content.get_outlines(title=idea, intro=intro)
    article = content.get_article(title=idea, intro=intro, outlines=outline)
    write(title="Article Title", body="Article Body")
    print("End")
