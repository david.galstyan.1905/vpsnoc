import requests
from requests import Response
from typing import Any


class Content:
    def __init__(self):
        self.__idea_url = "https://api.writesonic.com/v2/business/content/blog-ideas?engine=good&language=ru"

        self.__into_url = "https://api.writesonic.com/v2/business/content/blog-intros?engine=good&language=ru" \
                          "&num_copies=1"

        self.__outline_url = "https://api.writesonic.com/v2/business/content/blog-outlines?engine=good&language=ru" \
                             "&num_copies=1"
        self.__article_url = "https://api.writesonic.com/v2/business/content/ai-article-writer-v3?engine=good" \
                             "&language=ru"

        self.__headers: dict[str, str] = {
            "accept": "application/json",
            "content-type": "application/json",
            "X-API-KEY": "cd8fd496-eadc-45f8-8d92-80c098c4943d"
        }

    def get_ideas(self, topic: str) -> str:
        payload: dict[str, str] = {"topic": topic}
        response: Response = requests.post(self.__idea_url, json=payload, headers=self.__headers)
        ideas: list = response.json()
        idea: str = ideas[0].get("text")

        return idea

    def get_intro(self, title: str) -> str:
        payload: dict[str, str] = {"blog_title": title}
        response: Response = requests.post(self.__into_url, json=payload, headers=self.__headers)
        intros: list = response.json()
        intro: str = intros[0].get("text")

        return intro

    def get_outlines(self, title: str, intro: str) -> list[str]:
        payload: dict[str, str] = {
            "blog_intro": title,
            "blog_title": intro
        }

        response: Response = requests.post(self.__outline_url, json=payload, headers=self.__headers)
        outlines: list = response.json()
        outline = outlines[0].get("text")

        return outline

    def get_article(self, title: str, intro: str, outlines: list[str]) -> Any:
        payload = {
            "article_sections": outlines,
            "article_title": title,
            "article_intro": intro
        }

        response: Response = requests.post(self.__article_url, json=payload, headers=self.__headers)

        article = response.json()

        print(article)

        return article
