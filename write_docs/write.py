import os
from docx import Document


class Write:
    def __init__(self):
        self.__document: Document = Document()

        current_directory_path: str = os.getcwd()
        current_directory_files: list[str] = os.listdir(current_directory_path)

        if current_directory_files.count("vpsnoc.docx") >= 1:
            article_directory = current_directory_path + "\\vpsnoc.docx"
            os.remove(article_directory)

    def __call__(self, *args, **kwargs):
        self.__document.add_heading(kwargs.get("title"), 0)
        self.__document.add_paragraph(kwargs.get("body"), style='Intense Quote')

        self.__document.save('vpsnoc.docx')
